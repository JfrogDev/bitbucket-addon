FROM node:8

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Environment variables
ENV DATABASE_URL postgres://admin:password@postgresql:5432/bitbucket
ENV NODE_ENV development
ENV PORT 3000

# Install app dependencies
COPY . /usr/src/app/

RUN npm install -g npm@latest
RUN npm install

EXPOSE 3000
CMD [ "npm", "start" ]