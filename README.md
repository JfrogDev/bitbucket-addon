# **DEPRECATED** - This pipe will stop working after 1st May 2021. Instead, please use a more generic pipe available here [https://bitbucket.org/jfrog/jfrog-setup-cli/src/master/](https://bitbucket.org/jfrog/jfrog-setup-cli/src/master/).

## JFrog Add-on for Bitbucket Cloud
JFrog has developed an add-on for Atlassian�s Bitbucket Cloud. 
The add-on integrates Bitbucket Cloud with JFrog Artifactory and JFrog Bintray to provide a unified dashboard that visualizes the entire release pipeline - from commit to Bitbucket triggering the CI server to run a build, to promotion in Artifactory through the quality gates, until it is released for distribution through Bintray.

# Steps to build and deploy JFrog Add-on:

### Prerequisites Details
* [Docker](https://www.docker.com/)
* [Docker-compose](https://docs.docker.com/compose/)
* [Node](https://nodejs.org/)
* [Npm](https://github.com/npm/cli)

## Build and run Add-on using Docker-compose:
1.   `git clone https://JfrogDev@bitbucket.org/JfrogDev/bitbucket-addon.git`
2.   `cd bitbucket-addon`
3.   `docker-compose build`
4.   Update `AC_LOCAL_BASE_URL` value to a domain name where this add-on will be hosted. E.g `https://jfrog-addon.mycompany.com/`
     Note: Only Secure Domain name is supported.
5.   `docker-compose up -d`
6.   You should be able to access add-on on port 3000. It will redirect you to `$AC_LOCAL_BASE_URL/atlassian-connect.json`

# Installation

You can manually install/update/uninstall your add-ons from
`https://bitbucket.org/account/user/USERNAME/addon-management`.


# Getting Started

## Steps to Configure JFrog Add-on with Bitbucket Pipeline.

Assuming that pipeline is [configured](https://bitbucket.org/JfrogDev/) to build and publish build artifacts to Artifactory along with Build Information, 
let's configure Artifactory and Bintray credentials with JFrog Add-on to fetch information to create dashboard.

* Configure Artifactory User (Mandatory)
  Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Artifactory User
![Artifactory User Configuration](screenshots/artifactory-user.png)

    Note: Artifactory should be accessible from outside. You can use this add-on with AOL(Artifactory Online).

* Configure Bintray User (Mandatory)
  Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Bintray User
![Artifactory User Configuration](screenshots/bintray-user.png)

* Associate Bintray Package (Mandatory)
  Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Bintray Package
![Associate Bintray Package](screenshots/bintray-package.png)

### Once you have above steps configured, you should be able to see the Dashboard.
   Go to Repository -> Pipeline 
    
![Dashboard](screenshots/new_dashboard.png)

## Steps to Configure JFrog Add-on with Bamboo.

Assuming that Bamboo build is configured via [Bamboo Artifactory Plug-in](https://www.jfrog.com/confluence/display/RTF/Bamboo+Artifactory+Plug-in) to build and publish build artifacts to Artifactory along with Build Information,
let's configure Bamboo, Artifactory and Bintray credentials with JFrog Add-on to fetch information to create dashboard.

* Configure Bamboo User (Optional for BPAB stack)
    Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Bamboo User
![Bamboo User Configuration](screenshots/bamboo-user.png)

Note: Bamboo should be accessible form outside.

* Associate Bamboo Build (Optional for BPAB stack)
    Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Bamboo Build
![Associate Bamboo Build](screenshots/bamboo-build.png)

* Configure Artifactory User (Mandatory)
    Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Artifactory User
![Artifactory User Configuration](screenshots/artifactory-user.png)

    Note: Artifactory should be accessible form outside. You can use also this add-on with AOL(Artifactory Online). JFrog addo-on is only for Bitbucket Cloud.

* Associate Artifactory Build
    Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Artifactory Build
![Associate Artifactory Build](screenshots/artifactory-build.png)

* Configure Bintray User (Mandatory)
    Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Bintray User
![Artifactory User Configuration](screenshots/bintray-user.png)

* Associate Bintray Package (Mandatory)
    Go to Repository Settings -> BITBUCKET JFROG ADD-ON -> Associate Bintray Package
![Associate Bintray Package](screenshots/bintray-package.png)

### Once you have above steps configured, you should be able to see the Dashboard.
   Go to Repository -> Bamboo 
    
![Dashboard](screenshots/dashboard.png)


## Bitbucket Pipeline examples:

Example pipeline projects for Npm, Maven, Docker, Java, and other package types are available at [JfrogDev](https://bitbucket.org/JfrogDev/)

## For local development

### Prerequisites Details
* [Ngrok](https://ngrok.com/)

1. Run `npm install`.
2. Add your Bitbucket credentials to `credentials.json`.
3. Run `ngrok http 3000` and take note of the proxy's `https://THE_NGROK_BASE_URL`.
4. Run `AC_LOCAL_BASE_URL=https://THE_NGROK_BASE_URL PORT=3000 NODE_ENV=production node app.js` from the
repository root.
