// This is the entry point for your add-on, creating and configuring
// your add-on HTTP server

// [Express](http://expressjs.com/) is your friend -- it's the underlying
// web framework that `atlassian-connect-express` uses

var express = require('express');
var bodyParser = require('body-parser');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var morgan = require('morgan');
var Sequelize = require('sequelize');
var ac = require('atlassian-connect-express');

// Static expiry middleware to help serve static resources efficiently
process.env.PWD = process.env.PWD || process.cwd(); // Fix expiry on Windows :(
var expiry = require('static-expiry');
// We use [Handlebars](http://handlebarsjs.com/) as our view engine
// via [express-hbs](https://npmjs.org/package/express-hbs)
var hbs = require('express-hbs');
// We also need a few stock Node modules
var http = require('http');
var path = require('path');
var os = require('os');

// Anything in ./public is served up as static content
var staticDir = path.join(__dirname, 'public');
// Anything in ./views are HBS templates
var viewsDir = __dirname + '/views';
// Your routes live here; this is the C in MVC
var routes = require('./routes');
// Bootstrap Express
var app = express();
// Bootstrap the `atlassian-connect-express` library

var addon = ac(app, {
  config: {
    descriptorTransformer: (descriptor, config) => {
      descriptor.modules.oauthConsumer.clientId = config.consumerKey();
      return descriptor;
    }
  }
});

// You can set this in `config.json`
var port = addon.config.port();
// Declares the environment to use in `config.json`
var devEnv = app.get('env') == 'development';

// The following settings applies to all environments
app.set('port', port);

// Configure the Handlebars view engine
app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

// Declare any Express [middleware](http://expressjs.com/api.html#middleware) you'd like to use here
app.use(morgan(devEnv ? 'dev' : 'combined'));
// Include request parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use((req, res, next) => {
    if(req.originalUrl === "/installed"){
        req.body.baseUrl = req.body.baseApiUrl;
    }
    next();
});

// Gzip responses when appropriate
app.use(compression());
// You need to instantiate the `atlassian-connect-express` middleware in order to get its goodness for free
// Enable static resource fingerprinting for far future expires caching in production
app.use(expiry(app, {dir: staticDir, debug: devEnv}));
// Add an hbs helper to fingerprint static resource urls
hbs.registerHelper('furl', function(url){ return app.locals.furl(url); });
// Mount the static resource dir
app.use(express.static(staticDir));

// Show nicer errors when in dev mode
if (devEnv) app.use(errorHandler());

// Wire up your routes using the express and `atlassian-connect-express` objects
routes(app, addon);

hbs.registerHelper('furl', function (url) {
    return app.locals.furl(url);
});

var setEncryptionKey = function () {
    var encryptionKey = getEncryptionKey();
    encryptionKey.findOne().then(data => {
        if (data == null) {
            let newKey = new EncryptionKey({
                key: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
            });
            newKey.save(function (err) {
                if (err) {
                    console.log('Error occurred while creating a Encryption Key' + err.message);
                } else {
                    console.log('Artifactory: CREATING NEW ASSOCIATION OF ENCRYPTION KEY');
                }
            });
            global.encryptionKey = newKey;
        } else {
            global.encryptionKey = data.key;
        }
    })
};

hbs.registerHelper('compare', function (lvalue, operator, rvalue, options) {

    var operators, result;

    if (arguments.length < 3) {
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
    }

    if (options === undefined) {
        options = rvalue;
        rvalue = operator;
        operator = "===";
    }

    operators = {
        '==': function (l, r) { return l == r; },
        '===': function (l, r) { return l === r; },
        '!=': function (l, r) { return l != r; },
        '!==': function (l, r) { return l !== r; },
        '<': function (l, r) { return l < r; },
        '>': function (l, r) { return l > r; },
        '<=': function (l, r) { return l <= r; },
        '>=': function (l, r) { return l >= r; },
        'typeof': function (l, r) { return typeof l == r; }
    };

    if (!operators[operator]) {
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
    }

    result = operators[operator](lvalue, rvalue);

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }

});


var BintrayUser = addon.settings.schema.define('BintrayUser', {
    repoUuid: {type: Sequelize.STRING, allowNull: true},
    bintrayUsername: {type: Sequelize.STRING, allowNull: true},
    apiKey: {type: Sequelize.STRING, allowNull: true}
});
var BintrayUserTable = BintrayUser.sync();

var ArtifactoryUser = addon.settings.schema.define('ArtifactoryUser', {
    repoUuid: {type: Sequelize.STRING, allowNull: true},
    url: {type: Sequelize.STRING, allowNull: true},
    artifactoryUsername: {type: Sequelize.STRING, allowNull: true},
    password: {type: Sequelize.STRING, allowNull: true}
});
var ArtifactoryUserTable = ArtifactoryUser.sync();

var BambooUser = addon.settings.schema.define('BambooUser', {
    repoUuid: {type: Sequelize.STRING, allowNull: true},
    url: {type: Sequelize.STRING, allowNull: true},
    bambooUsername: {type: Sequelize.STRING, allowNull: true},
    password: {type: Sequelize.STRING, allowNull: true}
});
var BambooUserTable = BambooUser.sync();

var BintrayPackage = addon.settings.schema.define('BintrayPackage', {
    repoUuid: {type: Sequelize.STRING, allowNull: true},
    bintrayPackage: {type: Sequelize.STRING, allowNull: true}
});
var BintrayPackageTable = BintrayPackage.sync();

var BambooBuild = addon.settings.schema.define('BambooBuild', {
    repoUuid: {type: Sequelize.STRING, allowNull: true},
    bambooBuildName: {type: Sequelize.STRING, allowNull: true},
    bambooBuildKey: {type: Sequelize.STRING, allowNull: true}
});

var BambooBuildTable = BambooBuild.sync();

var ArtifactoryBuild = addon.settings.schema.define('ArtifactoryBuild', {
    repoUuid: {type: Sequelize.STRING, allowNull: true},
    artifactoryBuild: {type: Sequelize.STRING, allowNull: true}
});

var ArtifactoryBuildTable = ArtifactoryBuild.sync();

var EncryptionKey = addon.settings.schema.define('EncryptionKey', {
    key: {type: Sequelize.STRING, allowNull: false}
});

var EncryptionKeyTable = EncryptionKey.sync();

global.getBintrayUser = function () {
    return BintrayUser;
};

global.getArtifactoryUser = function () {
    return ArtifactoryUser;
};

global.getBambooUser = function () {
    return BambooUser;
};

global.getBintrayPackage = function () {
    return BintrayPackage;
};

global.getBambooBuild = function () {
    return BambooBuild;
};

global.getArtifactoryBuild = function () {
    return ArtifactoryBuild;
};

global.getEncryptionKey = function () {
    return EncryptionKey;
};

Promise.all([BintrayUserTable, BambooUserTable, ArtifactoryUserTable, ArtifactoryBuildTable, BambooBuildTable, BintrayPackageTable, EncryptionKeyTable]).then(function () {
    // Boot the damn thing
    setEncryptionKey();
    http.createServer(app).listen(port, function(){
        console.log('Add-on server running at http://' + os.hostname() + ':' + port);
    });
}).catch(function (e) {
    console.log("Error creating Schema" + e);
});

