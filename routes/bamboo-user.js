var crypto = require('./crypto-credentials');

var postBambUser = function (req, res) {
    var repoUuid = req.body.repoUuid;
    var url = req.body.url;
    var bambooUsername = req.body.username;
    var password = req.body.password;
    var BambooUser = getBambooUser();
    BambooUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var newUser = new BambooUser({
                repoUuid: repoUuid,
                url: url,
                bambooUsername: bambooUsername,
                password: crypto.encrypt(password)
            });
            newUser.save(function (err) {
                if (err) {
                    res.send('Error occurred while creating a new associated Bamboo user:' + err.message);
                } else {
                    console.log('Bamboo: CREATING NEW ASSOCIATION OF ' + repoUuid + ' WITH ' + bambooUsername);
                }
            });
        } else {
            data.url = url;
            data.bambooUsername = bambooUsername;
            data.password = crypto.encrypt(password);
            data.save(function (err) {
                if (err) {
                    res.send('Error occurred while updating associated Bamboo user:' + err.message);
                } else {
                    console.log('Bamboo: UPDATING EXISTING ASSOCIATION OF ' + repoUuid + ' WITH ' + bambooUsername);
                }
            });
        }
        res.render('bamboo-user', {username: bambooUsername, password: password, url: url, repoUuid: repoUuid});
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Bamboo user: ' + error);
    });
};

var getBambUser = function (req, res) {
    var repoUuid = req.query.repoUuid;

    var BambooUser = getBambooUser();
    BambooUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            res.render('bamboo-user', {url: null, username: null, password: null, repoUuid: repoUuid});
        } else {
            res.render('bamboo-user', {
                url: data.url,
                username: data.bambooUsername,
                password: crypto.decrypt(data.password),
                repoUuid: repoUuid
            })
        }
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Bamboo user: ' + error);
    });
};

module.exports = {
    getBambUser,
    postBambUser
};