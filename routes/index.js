var jwt = require('atlassian-jwt');
var ac = require('atlassian-connect-express');
var artUser = require('./artifactory-user');
var bmbUser = require('./bamboo-user');
var bintUser = require('./bintray-user');
var bintPackage = require('./associate-bintray-package');
var artBuild = require('./associate-artifactory-build');
var bmbBuild = require('./associate-bamboo-build');
var testConnection = require('./test-connection');
var dashboard = require('./bamboo-dashboard');
var newDashboard = require('./new-dashboard');
var downloadArtifacts = require('./download-artifacts');
var triggerBuild = require('./trigger-build');

module.exports = function (app, addon) {
    //healthcheck route used by micros to ensure the addon is running.
    app.get('/healthcheck', function (req, res) {
        res.sendStatus(200);
    });

    // Root route. This route will redirect to the add-on descriptor: `atlassian-connect.json`.
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    // Add any additional route handlers you need for views or REST resources here...
    app.post('/bintray-user', addon.checkValidToken(), fetchCurrentUsername(), bintUser.postBintUser);
    app.get('/bintray-user', addon.authenticate(), fetchCurrentUsername(), bintUser.getBintUser);

    app.post('/artifactory-user', addon.checkValidToken(), fetchCurrentUsername(), artUser.postArtUser);
    app.get('/artifactory-user', addon.authenticate(), fetchCurrentUsername(), artUser.getArtUser);

    app.post('/bamboo-user', addon.checkValidToken(), fetchCurrentUsername(), bmbUser.postBambUser);
    app.get('/bamboo-user', addon.authenticate(), fetchCurrentUsername(), bmbUser.getBambUser);

    app.get('/associate-bintray-package', addon.authenticate(), fetchCurrentUsername(), bintPackage.getBintPackage);
    app.post('/associate-bintray-package', addon.checkValidToken(), fetchCurrentUsername(), bintPackage.postBintPackage);

    app.get('/associate-bamboo-build', addon.authenticate(), fetchCurrentUsername(), bmbBuild.getBambBuild);
    app.post('/associate-bamboo-build', addon.checkValidToken(), fetchCurrentUsername(), bmbBuild.postBambBuild);

    app.get('/associate-artifactory-build', addon.authenticate(), fetchCurrentUsername(), artBuild.getArtBuild);
    app.post('/associate-artifactory-build', addon.checkValidToken(), fetchCurrentUsername(), artBuild.postArtBuild);

    app.get('/testArtifactoryConnection', testConnection.testArtifactoryConnection);
    app.get('/testBambooConnection', testConnection.testBambooConnection);

    app.get('/downloadArchive/:buildName/:buildNumber', addon.checkValidToken(), fetchCurrentUsername(), (req, res) => downloadArtifacts.download(req, res, addon));

    app.get('/triggerBuild/:planKey', addon.checkValidToken(), fetchCurrentUsername(), (req, res) => triggerBuild.trigger(req, res, addon));

    app.post('/triggerBuild', addon.authenticate(), function (req, res) {
        console.log("triggering bamboo build");
    });

    app.get('/jfrog', addon.authenticate(), fetchCurrentUsername(), dashboard.getDashboard);

    app.post('/jfrog', addon.authenticate(), fetchCurrentUsername(), dashboard.postDashboard);

    app.get('/newjfrog', addon.authenticate(), fetchCurrentUsername(), (req, res) => newDashboard.getDashboard(req, res, addon));

    app.post('/newjfrog', addon.authenticate(), fetchCurrentUsername(), newDashboard.postDashboard);

    app.post('/uninstalled', addon.authenticate(), fetchCurrentUsername(), function (req, res) {
        res.sendStatus(204);
    });

    // load any additional files you have in routes and apply those to the app
    {
        var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync("routes");
        for (var index in files) {
            var file = files[index];
            if (file === "index.js") continue;
            // skip non-javascript files
            if (path.extname(file) !== ".js") continue;

            var routes = require("./" + path.basename(file));

            if (typeof routes === "function") {
                routes(app, addon);
            }
        }
    }

    function getCurrentUsername(req, res, cb) {
        addon.loadClientInfo(req.context.clientKey)
            .then(function (clientInfo) {
                if (clientInfo == null) {
                    throw new Error('Current Username not found!')
                }
                cb(clientInfo.principal.username);
            })
            .then(undefined, function (error) {
                res.send('Failed to authenticate request: ' + error.message);
            });
    }

    function fetchCurrentUsername() {
        return function (req, res, next) {
            getCurrentUsername(req, res, function (currentUsername) {
                req.bitBucketUsername = currentUsername;
                next()
            })
        };
    }

};
