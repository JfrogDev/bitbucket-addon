var request = require('request');
var artBuild = require('./associate-artifactory-build');
var bintPackage = require('./associate-bintray-package');

var getDashboard = function (req, res, addon) {
    const repoPath = req.query.repoPath;
    const repoOwner = req.query.account;
    const repoUuid = req.query.repoUuid;
    const repoName = repoPath.split('/')[1];
    const httpClient = addon.httpClient({
        clientKey: req.context.clientKey,
        addonKey: req.context.addonKey
    });
    httpClient.get('/2.0/repositories/' + repoPath, function (err, apiRes, apiBody) {
        const branchName = JSON.parse(apiBody).mainbranch.name;
        console.debug("Branch Name :" + branchName);
        httpClient.get('/2.0/repositories/' + repoPath + '/src/' + branchName + '/bitbucket-pipelines.yml', function (err, pipelineRes, pipelineBody) {
            const pipelineEnabled = pipelineRes.statusCode;
            if (pipelineEnabled === 200) {
                console.debug("Pipeline is Enabled");
                httpClient.get('/2.0/repositories/' + repoPath + '/pipelines/?sort=-created_on', function (err, buildsRes, buildsBody) {
                    const builds = JSON.parse(buildsBody);
                    builds.values.forEach(build => build.name = repoOwner + '-' + repoName + '-' + branchName);
                    builds.values.forEach(build => build.url = 'https://bitbucket.org/' + repoPath + '/addon/pipelines/home#!/results/' + build.build_number);
                    var artRequestPromises = builds.values.map(build => makeArtRequestPromise(build, repoUuid));
                    var bintRequestPromises = artRequestPromises.map(promise => makeBintRequestPromise(promise, repoUuid));

                    Promise.all(bintRequestPromises).then(data => {
                        res.render('newjfrog', {'builds': builds, 'repoUuid': repoUuid});
                    }).catch(function (error) {
                        const message = error;
                        res.render('error', {message: message});
                    });
                });
            } else {
                console.log("Pipeline is not Enabled");
                const message = 'Pipeline is not Enabled! ';
                res.render('error', {message: message});
            }
        });
    });
};

var postDashboard = function (req, res) {
    console.log("This API is not implemented");
    const message = 'This API is not implemented! ';
    res.render('error', {message: message});
};


const makeArtRequestPromise = (build, repoUuid) => {
    return new Promise((resolve, reject) => {
        artBuild.artifactoryRequestOptions('build/' + build.name + '/' + build.build_number, repoUuid, function (options, error) {
            if (options != null && !error) {
                function callback(error, response, body) {
                    if (!error && response.statusCode === 200) {
                        var list = JSON.parse(body);
                        (list.uri) ? build.buildUri = list.uri.replace("api/build", "webapp/#/builds") : list.uri;
                        build.buildInfo = list.buildInfo;
                        resolve(build);
                    } else {
                        resolve(build);
                    }
                }

                request(options, callback);
            } else if (error) {
                console.log('Error: ' + error);
                reject(error)
            } else {
                console.log('Artifactory User not found');
                reject('Artifactory User not found')
            }
        });
    });
};

const makeBintRequestPromise = (promise, repoUuid) => {

    return new Promise((resolve, reject) => {
        promise.then(build => {
            var BintrayPackage = getBintrayPackage();
            BintrayPackage.findOne({where: {repoUuid: repoUuid}}).then(data => {
                if ((data == null) || (data.bintrayPackage == null)) {
                    console.log("Package not found!");
                    reject("Package not found");
                } else {
                    const post_data = '[{"build.number":["' + build.build_number + '"]},{"build.name":["' + build.name + '"]}]';
                    const path = 'search/attributes/' + data.bintrayPackage + '/versions';
                    bintPackage.bintrayRequestOptionsPost(path, repoUuid, post_data, function (options) {
                        if (options != null) {
                            function callback(error, response, body) {
                                if (!error && response.statusCode === 200) {
                                    build.bintrayInfo = JSON.parse(body);
                                    resolve(build);
                                } else {
                                    resolve(build);
                                }
                            }

                            request(options, callback);
                        }
                    });
                }
            }).catch(function (error) {
                console.log('Error occurred while querying for an already existing associated Bintray package: ' + error);
            });

        }).catch(function (error) {
            console.log("Error" + error);
            reject(error);
        });
    });
};


module.exports = {
    getDashboard,
    postDashboard
};




